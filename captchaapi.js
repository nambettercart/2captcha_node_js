const request = require('request-promise')

const getAnswer = (id, api) => {
    return new Promise((resolve, reject) => {
        const polling = setInterval(() => {
            request.get({
                uri: `${api.res}?key=${api.key}&json=1&action=get&id=${id}`,
                json: true
            })
            .then((res) => {
                if (res.status === 1) {
                    clearInterval(polling)
                    resolve(res.request)
                } else if (res.request !== 'CAPCHA_NOT_READY') {
                    clearInterval(polling)
                    reject(res.request)
                }
            })
            .catch((error) => {
                clearInterval(polling)
                reject(error)
            })
        }, api.pollingInterval)
    })
}

const submitCaptchaV2 = (captchaParams, api) => {
    return new Promise((resolve, reject) => {
        captchaParams.soft_id = '2935';
        var requestUrl = api.in + "?key=" + api.key ;
        requestUrl = requestUrl + "&method=" + captchaParams.method;

        if(captchaParams.proxy != null){
            requestUrl = requestUrl + "&proxy=" + captchaParams.proxy;
        }

        if(captchaParams.proxyType != null){
            requestUrl = requestUrl + "&proxytype=" + captchaParams.proxyType;
        }

        if(captchaParams.action != null){
            requestUrl = requestUrl + "&action=" + captchaParams.action;
        }

        if(captchaParams.version != null){
            requestUrl = requestUrl + "&version=" + captchaParams.version;
        }

        if(captchaParams.sitekey != null){
            requestUrl = requestUrl + "&sitekey=" + captchaParams.sitekey;
        }

        if(captchaParams.pageurl != null){
            requestUrl = requestUrl + "&pageurl=" + captchaParams.pageurl;
        }

        if(captchaParams.googlekey != null){
            requestUrl = requestUrl + "&googlekey=" + captchaParams.googlekey ;
        }


        if(captchaParams.min_score != null){
            requestUrl = requestUrl + "&min_score=" + captchaParams.min_score;
        }

        if(captchaParams.invisible != null){
            requestUrl = requestUrl + "&invisible=" + captchaParams.invisible;
        }

        if(captchaParams.header_acao != null){
            requestUrl = requestUrl + "&header_acao=" + captchaParams.header_acao;
        }


        request.post(requestUrl, {
            json: true,
            body: (typeof captchaParams === 'object') ? captchaParams : null
        })
        .then((res) => {
            if (res.status === 1) {
                resolve(res.request)
            } else {
                reject(res.request)
            }
        })
        .catch((error) => {
            reject({ error })
        })
    })
}

const solveCaptchaV2 = async (params, api) => {
    try {
        let id = await submitCaptchaV2(params, api)
        let answer = await getAnswer(id, api)
        return answer
    } catch (e) {
        return e
    }
}

module.exports = {
    getAnswer,
    solveCaptchaV2
}